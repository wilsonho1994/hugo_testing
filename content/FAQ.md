+++
Title = "FAQ"

+++
##### Why the connections are still rejected after adding into allowlist?

Connection filterings such as SPF Verification and IP Reputation can only be bypassed using the Connection IP Allowlist. Meanwhile, adding entries into Sender Domain Allowlist or Sender Address Allowlist will not prevent messages from being rejected at connection level.

##### Why the emails are still quarantined after adding into allowlist?

There are two potential reasons why messages are being quarantined after being added to the Allowlist.

1\. Malicious Email

Allowlist can only bypass **Spam** and **Graymail** filtering. Malicious emails can still be quarantined due to other filtering mechanisms like the virus signature comparison.

2\. Envelope Sender

Allowlist is setup based on the From Header. Filters will not be bypassed for messages that the Envelope Sender is being added into the allowlist instead.