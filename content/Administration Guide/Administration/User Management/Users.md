+++
Title = "Users"

+++
Users record all the existing user accounts in Green Radar grMail. Administrators can manage the personal allowlist and blocklist of each user account through the Users tab.

New user accounts can be created through different channels

1. Administrators create new user accounts through the Users tab
2. Users self-register their own local accounts through the portal interface
3. Users log in to the portal interface using other authentication methods
   * IMAP Authentication
   * SAML Authentication
   * Third-Parties OAuth

{{< hint type="important" >}}

Green Radar grMail accepts all emails for the registered domain by default. It is not required to create the user account in Green Radar grMail in order to receive emails. 

{{< /hint >}}