+++
Title = "DNS Status"

+++
DNS Status regularly queries all the registered domain DNS records and checks for any potential misconfiguration. If DNS record misconfiguration is found, the suggested correction will be provided.

DNS Status examines two types of DNS records.

* MX Record
* SPF Record (Applicable for Outbound Module)

<!-- spellchecker-disable -->

{{< hint type="note">}}

DNS records are automatically retrieved from public DNS servers regularly. However, manual retrieval can be initiated by clicking the **Refresh** button

{{< /hint >}}

<!-- spellchecker-enable -->