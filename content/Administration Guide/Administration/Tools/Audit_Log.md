+++
Title = "Audit Log"

+++
Audit Log records all the action being taken inside the Green Radar grMail portal interface. Enabling action traces for auditing purposes.

<!-- spellchecker-disable -->

{{< hint type="note">}}

Audit Logs are being kept up to 6 months before discard

{{< /hint >}}

<!-- spellchecker-enable -->