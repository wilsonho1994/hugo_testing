+++
Title = "Connection Test"

+++
Connection Test is a troubleshooting tool to verify if the connection from Green Radar grMail to a specific destination or mailbox can be successfully established.

### Network Connection Verification

Below are the steps for verifying the network connection between Green Radar grMail and a specific destination

1. Navigate to Administration > Tools > Connection Test
2. Enter the Hostname or IP address of the target server
3. Modify the TCP if standard SMTP Port 25 is not being used
4. Select whether initiate the SMTP connection with TLS encryption
5. Click the **Test** button
6. Review the Session Transcript for the testing result

### Send Testing Email

Below are the steps for sending out testing emails from Green Radar grMail to a specific mailbox.

1. Navigate to Administration > Tools > Connection Test
2. Enter the Hostname or IP address of the target server
3. Enter the recipient's email address
4. Modify the TCP if standard SMTP Port 25 is not being used
5. Select whether initiate the SMTP connection with TLS encryption
6. Tick the **Send Mail** checkbox
7. Click the **Test** button
8. Review the Session Transcript for the testing result