+++
Title = "Contact"

+++
Contact records the contact methods for both the technical contact point and the business contact point. Notifications such as functionality updates and license renewal will be sent based on the recorded contact information.