+++
Title = "License Subscription"

+++
License Subscription provides an overview of the license subscription status for the tenant.

* Tenant Name
* Number of Subscribed Licenses for Professional Edition
* Number of Subscribed Licenses for Advanced Edition
* License Expiry Date
* List of Registered Domains