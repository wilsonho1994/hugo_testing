+++
Title = "URL Isolation Allowlist"

+++
URL Isolation Allowlist is a list of URL domains used for bypassing the Isolation URL conversion. When the URL Isolation feature is enabled, all URLs inside email content will be converted to Isolation URLs except for those globally whitelisted by Green Radar. However, specific URL domains can also be bypassed by specifying the corresponding entries in the URL Isolation Allowlist tab.

### New Allowlist Entry Creation

1. Navigate to Advanced Module > URL Isolation > URL Isolation Allowlist
2. Click the **Add** button at the top right corner
3. Enter the domain inside the **Target** field
4. Select the **Sub-Domain** flag

   **True**: Apply the whitelist action to all sub-domains

   **False**: Only apply the whitelist action to the root domain
5. Click the **Add** button
6. Click the **Save** button