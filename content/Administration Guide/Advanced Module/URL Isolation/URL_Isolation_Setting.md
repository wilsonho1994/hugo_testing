+++
Title = "URL Isolation Setting"

+++
In this page, changes the settings on how the URL Isolation will work on an URL that are associated with the domain takes place.

For the scope, there are two mailbox settings which are **All Mailbox Transform** and **Partial Mailbox Transform** respectively. For all mailbox transform option, all email addresses with the domain name of the domain will transform the URL. For the partial mailbox transform, we can add and choose specific email addresses associated with the domain that we want to transform.

For the transform policy, there are two options which are **Enforce** and **Optional**. For the enforce option, the URL Isolation will isolate the URL automatically. For the optional, there is a message where it gives an option for the user to choose to have the URL isolated or not.