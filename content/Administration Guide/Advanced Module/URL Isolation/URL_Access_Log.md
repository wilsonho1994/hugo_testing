+++
Title = "URL Access Log"

+++
URL Access Log records the activity logs when the Isolation URLs are clicked. Detailed information about the URLs being clicked can be traced inside the URL Access Logs tab.

* **Date**

  Datetime of the URL being clicked
* **URL**

  The URL that being clicked
* **User**

  The user that clicked the URL
* **Risk Level**

  **High Risk**: Block users from accessing the website

  **Medium Risk**: Continue to the website but disable text input by default

  **Low Risk**: Continue to the website

  **Invalid URL**: Corrupted URLs that cannot be processed
* **User Agent**

  The web browser that the user used to open the URLs
* **Connection IP**

  The WAN IP address connecting to the Green Radar Isolation platform for accessing the website
* **Action**

  **Isolate**: Users navigate to the website through the Green Radar Isolation platform

  **Bypass**: Users navigate to the website directly

  **Whitelisted**: URLs matched with the URL Isolation Allowlist