+++
Title = "URL Isolation"

+++
URL Isolation feature isolates threats away from endpoints and business networks by rewriting URLs inside email content to Green Radar isolation servers URLs. Detail controls over the URL rewriting mechanism can be configured inside URL Isolation tab.

<!-- spellchecker-disable -->

{{< toc-tree >}}

<!-- spellchecker-enable -->