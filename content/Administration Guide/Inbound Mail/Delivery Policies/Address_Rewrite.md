+++
Title = "Address Rewriting"

+++
Address Rewriting is a set of rules for rewriting the Envelope Recipient Address or Domain. Each message is checked against available rules and the Envelope Recipient Address or Domain will be rewritten if a matching case is found.

### Create New Address Rewriting Rules

Below are the steps to manually create new address rewriting rules.

1. Navigate to _Inbound Mail > Delivery Policies > Address Rewriting_
2. Click the **Add** button at the top right corner
3. Select the **Type** of the rewriting rule
   * **Address**: Rewrite an email address to another email address

     E.g. user1@sample.com > user2@sample.com
   * **Domain**: Rewrite an email domain to another email domain

     E.g. user@sample1.com > user@sample2.com
4. Specify the **Original Recipient**
5. Specify the **Rewrite Recipient**
6. Click the **Confirm** button at the bottom right corner

<!-- spellchecker-disable -->

{{< hint type="important" >}}

External email addresses or domains cannot be specified inside the **Rewrite Recipient** field.

{{< /hint >}}

{{< hint type="important" >}}

Type **Domain** can only be selected for tenants that own multiple domains.

{{< /hint >}}

<!-- spellchecker-enable -->