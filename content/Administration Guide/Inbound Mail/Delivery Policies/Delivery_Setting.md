+++
Title = "Delivery Setting"

+++
Inbound Delivery Setting controls the message delivery mechanism from Green Radar grMail system to the client mail server.

### Delivery Hosts

List of client mail server hostname(s) or IP address(es) for the message delivery with priority from top to bottom.

### Delivery Mechanism

* **Round Robin**

  Deliver messages to each delivery host one at a time, sharing the load with every single delivery host.
* **Static**

  Deliver messages to the delivery host with the highest priority. Only try to deliver messages to other delivery hosts according to the priority if the highest priority host is not available.