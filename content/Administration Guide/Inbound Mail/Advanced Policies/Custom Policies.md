+++
Title = "Inbound Custom Policies"

+++
Inbound Custom Policies is a customizable policy framework with predefined conditions and actions. Domain-specific policies can be self-defined for the inbound mail filtering process using the Custom Policies tab.

### Supported Conditions

* Envelope Sender
* From Header
* Sender Display Name
* Envelope Recipient
* To Header
* Subject
* Attachment File Extension
* Connection IP

### Supported Actions

* **Quarantine**

  Quarantine if the messages matched the conditions
* **Quarantine (Lock)**

  Quarantine if the messages matched the conditions and lock up the message to prevent users from releasing the messages
* **Copy To**

  BCC a copy of the messages to specific recipients if the messages matched the conditions
* **Append X-Header**

  Append a specific X-Header to the message header if the messages matched the conditions
* **Bypass**

  Bypass specific filtering modules for the messages if the messages matched the conditions