+++
Title = "Quarantined Report"

+++
Quarantined Reports are system-generated notification emails when there are messages being quarantined by Green Radar grMail. Domain-specific detail mechanisms of the quarantine report can be configured inside the Quarantine Report tab.

### Quarantine Report Setting

* **Enable**

  Enable or Disable quarantine report generation
* **Scope**

  **All**: Generate quarantine reports for all users

  **Partial**: Generate quarantine reports for listed users or groups only

  **Exclusion**: Generate quarantine reports for all users except for listed users or groups
* **Copy Policy**

  Generate an extra copy of quarantine reports to specific users or groups. The quarantine report owner still receives his own copy of the quarantine reports
* **Forward Policy**

  Forward the quarantine reports to specific users or groups. The quarantine report owner will not receive his own copy of the quarantine reports.
* **Scheduling**

  Timestamp for the quarantine report generation
* **Quarantine Reason**

  Categories of the messages that would be included inside the quarantine report
* **Languages**

  Language of the quarantine report template
* **Authentication**

  Whether users are required to authenticate before previewing and releasing the emails through quarantine report