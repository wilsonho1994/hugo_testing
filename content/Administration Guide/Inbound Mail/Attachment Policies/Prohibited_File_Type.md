+++
Title = "Prohibited File Type"

+++
Green Radar grMail prohibited a set of attachments based on true file type and file extensions. By default, all the messages with attachments matched with the prohibited list will be quarantined. However, the handling mechanism can be switched inside the Prohibited File Type tab based on the file extension.