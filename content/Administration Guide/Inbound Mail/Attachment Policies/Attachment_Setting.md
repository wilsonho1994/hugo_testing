+++
Title = "Attachment Setting"

+++
Attachment Setting is a set of customizable policies regarding the handling mechanism of different attachment categories.

### Attachment Categories

* **Office Documents with Macro File**

  Microsoft Office documents containing built-in macros script files
* **Password Protected Attachments**

  Password Protected Microsoft Office documents, PDF files, and compressed archives
* **Uncrackable Password Protected Attachments**

  Password Protected Microsoft Office documents, PDF files, and compressed archives that cannot be cracked open by commonly used passwords and phrases extracted from message content
* **Unknown File Type**

  Attachments with true file types and extensions that are yet to be classified
* **Malformed Attachments**

  Attachments that cannot be processed

### Handling Mechanism

* **Quarantine**

  Quarantine the messages attached with matched attachments
* **Deliver**

  Continue the standard filtering flow