+++
Title = "Allowlist"

+++
Inbound Allowlist is a set of lists with different trusted email attributes for the entire domain.

### Supported Email Attributes

* Sender Domain
* Sender Address
* Connection IP

{{< hint type="important" >}}

Messages with the matched email attributes will only bypass the **Spam** & **Graymail** filtering. Rejected emails can only be accepted by specifying the **Source IP Address** inside the Allowlist.

{{< /hint >}}

### Manually Create New Allowlist Entry

Below are the steps to manually create new allowlist entries.

1. Navigate to the corresponding allowlist tab
2. Click the **Add** button at the top right corner
3. Input the new entries inside the **Target** field
4. Click the **Add** button at the end of the **Target** field
5. Click the **Confirm** button at the bottom right corner

{{< hint type="important" >}}

Allowlist configurations are applied for domains individually. For tenants that own multiple domains and would like to publish the same configuration to all the domains within the tenant, enable the **Apply to All Domains** option before saving the configuration.

{{< /hint >}}

### Import New Allowlist Entry

Instead of manually creating allowlist entries, the configuration can be imported through a configuration file. Below is the required format of the configuration file.

* Must be in .txt format
* One entry per line
* Maximum 500 entries per file