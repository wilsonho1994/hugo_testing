+++
Title = "VIP Watchlist"

+++
VIP Watchlist is a set of **Sender Display Name** entries for senior executives. Messages with a similar Sender Display Name as the VIP Watchlist entries will be processed by additional anti-spoofing policies and have a higher chance of being quarantined.

In the case of senior executives using personal mailboxes to communicate with company staff. Personal email address(es) can be defined for each VIP name to prevent legitimate email conversations between senior executives and company staff from being quarantined.

{{< hint type="important" >}}

Each domain can only specify a maximum of 10 VIP Watchlist entries. Please contact your account representative if more VIP Watchlist entries quota is required.

{{< /hint >}}

### Manually Create VIP Watchlist Entry

Below are the steps to manually create new VIP watchlist entries.

1. Navigate to _Inbound Mail > Watchlist > VIP Watchlist_
2. Click the **Add** button at the top right corner
3. Input the new entries inside the **Target** field
4. Click the **Add** button at the end of the **Target** field
5. Click the **Confirm** button at the bottom right corner

{{< hint type="important" >}}

VIP Watchlist configurations are applied for domains individually. For tenants that own multiple domains and would like to publish the same configuration to all the domains within the tenant, enable the **Apply to All Domains** option before saving the configuration.

{{< /hint >}}

### Import New VIP Watchlist Entry

Instead of manually creating VIP Watchlist entries, the configuration can be imported through a configuration file. Below is the required format of the configuration file.

* Must be in .txt format
* One entry per line
* Maximum 500 entries per file