+++
Title = "Reports"

+++
Reports are automatically generated on a monthly basis, containing general statistics of the tenant email traffic.

Instead of downloading the reports that are automatically generated on a monthly basis, users can customize their own reports by specifying the calculation time interval and the scope of the calculation.

Customize reports will be generated at the backend and available for download inside the Data Export tab once the generation is completed.