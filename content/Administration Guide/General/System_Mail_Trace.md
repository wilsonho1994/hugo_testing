+++
Title = "System Mail Trace"

+++
System Mail Trace records all the system emails generated and sent out by Green Radar grMail. Emails can be searched and filtered in the list through Advanced Search.

### Quarantine Report

* **User:** Owner of the Quarantine Report
* **Recipient:** Recipient of the Quarantine Report
* **Status:** Status of the Quarantine Report Delivery
* **Delivered:** Successfully delivered to the mail server specified inside Delivery Hosts
* **Bounced:** Mail server specified inside Delivery Hosts refuses to accept the message