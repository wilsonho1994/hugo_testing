+++
Title = "General"

+++
General tab provides monitoring interface for mail traffics, status of the mail traffic can be reviewed through dashboards, reporting and tracing specific emails.

<!-- spellchecker-disable -->

{{< toc-tree >}}

<!-- spellchecker-enable -->