+++
Title = "Delivered"

+++
The Delivered tab in both the Inbound and Outbound Mail Trace are legitimate emails that successfully pass through all the filters in Green Radar grMail and are distributed to the recipient. You can search and filter emails in the list through Advanced Search.

### Advanced Research

* **Date Range:** Today, Yesterday, Last 7 Days, Last 30 Days, This Month, Last Month, and Custom(Up to 6 Months) to select the time interval for searching emails.
* **From:** Partial or the complete sender’s email address.
* **To:** Partial or the complete recipient’s email address.
* **Subject:** Keyword of the subject of the email
* **Connection IP:** Connection IP address of the last mail server to Green Radar grMail.

{{< hint type="important" >}}

Samples of the missing case (False Negative) must be provided to Green Radar Support Team when reporting for any missing case. Since Green Radar grMail does not store any copy of the delivered emails

{{< /hint >}}