+++
Title = "Released"

+++
The Released tab in both the Inbound and Outbound Mail Trace are emails that were previously quarantined and later released to the recipient. Emails can be searched and filtered in the list through Advanced Search.

### Advanced Research

* **Date Range:** Today, Yesterday, Last 7 Days, Last 30 Days, This Month, Last Month, and Custom(Up to 6 Months) to select the time interval for searching emails.
* **From:** Partial or the complete sender’s email address.
* **To:** Partial or the complete recipient’s email address.
* **Subject:** Keyword of the subject of the email
* **Connection IP:** Connection IP address of the last mail server to Green Radar grMail.