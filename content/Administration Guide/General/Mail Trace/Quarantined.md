+++
Title = "Quarantined"

+++
The Quarantined tab in both the Inbound and Outbound Mail Trace are Spam/Phishing/Malicious emails that are being quarantined by the Green Radar grMail. You can search and filter emails in the list through Advanced Search.

### Advanced Research

* **Date Range:** Today, Yesterday, Last 7 Days, Last 30 Days, This Month, Last Month, and Custom(Up to 6 Months) to select the time interval for searching emails.
* **From:** Partial or the complete sender’s email address.
* **To:** Partial or the complete recipient’s email address.
* **Subject:** Keyword of the subject of the email
* **Connection IP:** Connection IP address of the last mail server to Green Radar grMail.

{{< hint type="important" >}}

Green Radar grMail stores a copy of the quarantined emails for 30 Days for users to preview and release the email if necessary. Copy of the quarantined emails will be discarded after 30 Days.

{{< /hint >}}