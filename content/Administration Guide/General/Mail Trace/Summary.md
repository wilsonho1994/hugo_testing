+++
Title = "Summary"

+++
The Summary page in both the Inbound and Outbound Mail Trace consists of all mails for every status. Emails can be searched and filtered in the list through Advanced Search by selecting date range, From, To, Subject, and Client IP.

### Advanced Search

* **Date Range:** click date range to open the drop-down window for Today, Yesterday, Last 7 Days, Last 30 Days, This Month, Last Month and Custom(date range in last 30 days) to select the amount of time for displaying emails.
* **From:** type a keyword of the sender’s email address.
* **To:** type a keyword of the recipient’s email address.
* **Subject:** type a keyword of the subject of the email
* **Client IP:** type the value of the client's IP address.

{{< hint type="important" >}}

For reporting emails, a problem can be selected and reported to GreenRadar for investigation by clicking the "**Report to Us**" button. The details of the email can also be seen by clicking the "**Preview**" button for information like email header and delivery status.

{{< /hint >}}