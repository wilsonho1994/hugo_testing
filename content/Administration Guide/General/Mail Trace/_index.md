+++
Title = "Mail Trace"

+++
Mail Trace records the detail information of each email received or connection established to Green Radar grMail. Email can be traced, previewed, released or submit for further analysis inside the Mail Trace tab.

<!-- spellchecker-disable -->

{{< toc-tree >}}

<!-- spellchecker-enable -->