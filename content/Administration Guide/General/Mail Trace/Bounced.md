+++
Title = "Bounced"

+++
The Bounced tab in both the Inbound and Outbound Mail Trace are emails that the mail server(s) specified inside the Delivery Host setting refuses to receive the email distributed by Green Radar grMail. You can search and filter emails in the list through Advanced Search by selecting Date Range, From, To, Subject, and Connection IP.

### Advanced Search

* **Date Range:** Today, Yesterday, Last 7 Days, Last 30 Days, This Month, Last Month, and Custom(Up to 6 Months) to select the time interval for searching emails.
* **From:** Partial or the complete sender’s email address.
* **To:** Partial or the complete recipient’s email address.
* **Subject:** Keyword of the subject of the email
* **Connection IP:** Connection IP address of the last mail server to Green Radar grMail.

### Common Bounced Reasons:

* **Unknown User:** Users do not exist in the mail server specified inside the Delivery Host setting
* **Message Oversize:** Message size over the limit configured in the mail server specified inside the Delivery Host

{{< hint type="important" >}}

Bounced emails are caused by the mail server(s) specified inside the Deliery Host, NOT by Green Radar grMail.

Green Radar grMail will generate an NDR message back to the sender if the message is bounced.

{{< /hint >}}