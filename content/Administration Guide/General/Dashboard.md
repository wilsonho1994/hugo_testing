+++
Title = "Dashboard"

+++
Dashboard provides an overview of the email quarantine status for both the inbound and outbound mail of the domain. :

1. Email Status (All Emails, Rejected, Quarantined, Released, Delivered and Bounced Email).
2. Mail Volume (Different email status are selective to display the volume of each type)
3. Threat Origin (Gives you the world map where it circles the places in blue where threats are being detected)
4. Quarantine Mail Breakdown (Breakdown of different quarantine reasons)
5. Rejected Connections Breakdown (Breakdown of different rejected connection reasons)
6. Top 10 Sender Domains
7. Top 10 Quarantined Domains
8. Top 10 Recipient Address
9. Top 10 Quarantined Recipient Address

{{< hint type="important" >}}

Click Date Range at the top right side of the page. Options of Last 24 hours, Last 7 Days, Last 30 Days are displayed in the drop-down window for filtering emails statistics.

{{< /hint >}}