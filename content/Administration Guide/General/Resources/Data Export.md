+++
Title = "Data Export"

+++
The Data Export tab in the Resources consolidates all the data exports, showing the status of the data exports and providing an interface for users to download the exported files.

### Data Exports

* Mail Log Export
* Rejected Connection Log Export
* URL Access Log
* Audit Log Export
* Customized Report

{{< hint type="important" >}}

The maximum number of Data Exports that can be saved simultaneously is 50. Old Data Exports must be deleted before generating new Data Export if the limit is reached.

{{< /hint >}}