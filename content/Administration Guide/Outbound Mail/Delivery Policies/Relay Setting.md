+++
Title = "Relay Setting"

+++
Outbound Relay Setting is a list of identity recognization methods for outbound mail processing. At least one relay setting must be selected in order to enable outbound mail processing. Only messages received through the selected channel will be accepted for outbound mail processing.

### Trusted Relay Hosts

* Static IP Address
* Microsoft 365
* Google Workspace

<!-- spellchecker-disable -->

{{< hint type="note">}}

IP ranges for Microsoft 365 and Google Workspace will be automatically adjusted

{{< /hint >}}

<!-- spellchecker-enable -->