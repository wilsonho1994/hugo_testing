+++
Title = "Outound Advanced Setting"

+++
Outbound Advanced Setting is a list of advanced customizable settings for outbound mail processing.

* **Disclaimer**

  **On**: Enable the Disclaimer feature for outbound mail processing

  **Off**: Disable the Disclaimer feature for outbound mail processing
* **Disclaimer Header**

  HTML-based annotation that would append to the beginning of the message content
* **Disclaimer Footer**

  HTML-based annotation that would append to the end of the message content

<!-- spellchecker-disable -->

{{< hint type="note">}}

HTML codes can be directly imported to the Disclaimer editor by clicking the code block button at the top-left corner

Disclaimer editor would auto-correct any syntax errors detected inside the HTML codes

{{< /hint >}}

<!-- spellchecker-enable -->