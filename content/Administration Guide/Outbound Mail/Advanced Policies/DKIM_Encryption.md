+++
Title = "DKIM Signature"

+++
DKIM Signature controls the DKIM signature signing mechanism and the DKIM private key management. DKIM private keys can be generated, imported, and selected as the signature key inside the DKIM Signature tab.

### Enable DKIM Signature Signing

Below are the steps to enable DKIM Signature Signing.

1. Navigate to the Outbound Mail > Advanced Policies > DKIM Signature
2. Toggle **DKIM Signature** to **ON**
3. Select the **Signature Key**
4. Click the **Save** button at the top right corner

### Generate a New DKIM Key Pair

Below are the steps to generate a new DKIM Key pair.

1. Navigate to the Outbound Mail > Advanced Policies > DKIM Signature
2. Click the **Add** button next to DKIM Keys
3. Enter the **Selector Name** and select the **Key Size**
4. Click the **Save** button

### Import an Existing DKIM Private Key

Below are the steps to import an existing DKIM Private Key.

1. Navigate to the Outbound Mail > Advanced Policies > DKIM Signature
2. Click the **Import** button next to DKIM Keys
3. Enter the **Selector Name** and select the **Key Size**
4. Click the **Browse** button and select the Key file from the PC
5. Click the **Save** button

<!-- spellchecker-disable -->

{{< hint type="note">}}

DKIM Private Key can be validated with the DKIM Public Key published in the DNS record through the **Validate** button

{{< /hint >}}

<!-- spellchecker-enable -->